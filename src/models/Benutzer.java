package models;

import java.io.Serializable;

public class Benutzer extends Human implements Serializable {

//    public Benutzer(String name, int age, String sex) {
//        super(name, age, sex);
//    }

    public void getLogin(){
        System.out.println("your now logged in, lol ! " +username);
    }
    public String username;

    public Benutzer(String name, int age, String sex, String username) {
        super(name, age, sex);
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
