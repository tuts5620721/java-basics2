package lambdas;

public class CalculatorImpl {

/*    @Override
    public void switchOn(){System.out.println("Switch ON !");
    }*/

    public static void main(String[] args) {
/*
        Calculator calculator = () -> System.out.println("Switched ON !");
        calculator.switchOn();
*/

/*        Calculator calculator = (input) -> System.out.println("Input: "+input);
        calculator.sum(16);*/

        // falls i1 kleiner als i2 werfe Fehler
        /*Calculator calculator1= (i1, i2 ) -> {
            if (i1 < i2) {
                throw  new RuntimeException("Fehler bei der Eingabe !");
            } else {
                return i1 - i2 || i1 * i2;
            }
        };
        System.out.println(calculator1.subtract(10, 12));*/

        //geht noch kürzer
        //Calculator calculatorSubract = (i1, i2 ) -> i1 - i2;
        //System.out.println(calculatorSubract.subtract(10, 25));

        Calculator calculatorMultiply = (a,b) -> a * b;
        System.out.println(calculatorMultiply.multiply(4,7));



    }
}
//  () -> {body};

