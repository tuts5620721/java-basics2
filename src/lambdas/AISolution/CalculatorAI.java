package lambdas.AISolution;

interface Calculator {
    int calculate(int a, int b, Operation operation);

    interface Operation {
        int apply(int a, int b);
    }
}