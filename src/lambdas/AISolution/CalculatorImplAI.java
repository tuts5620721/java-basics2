package lambdas.AISolution;

class CalcImpl implements Calculator {
    @Override
    public int calculate(int a, int b, Operation operation) {
        return operation.apply(a, b);
    }

    public static void main(String[] args) {
        // Create an instance of the implementation class
        CalcImpl calculator = new CalcImpl();

        // Generate some random numbers for testing
        int num1 = (int) (Math.random() * 100);
        int num2 = (int) (Math.random() * 100);

        // Perform multiplication using lambda
        int resultMultiply = calculator.calculate(num1, num2, (x, y) -> x * y);
        System.out.println("Multiplication Result: " + num1 + " * " + num2 + " = " + resultMultiply);

        // Perform subtraction using lambda
        int resultSubtract = calculator.calculate(num1, num2, (x, y) -> x - y);
        System.out.println("Subtraction Result: " + num1 + " - " + num2 + " = " + resultSubtract);
    }
}