package lambdas;

interface Calculator {

/*
    void switchOn();
*/

/*
    void sum(int input);
*/

    int subtract(int i1, int i2);

    int multiply(int a, int b);

}
