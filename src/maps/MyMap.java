package maps;

import java.util.HashMap;

public class MyMap {

    public static void main(String[] args) {

        /*HashMap<Student, Integer> studenten = new HashMap<>();
        studenten.put(,1);
        */

        HashMap<String, Integer> studIds = new HashMap<>();
        studIds.put("Sarah", 1);
        studIds.put("Jack", 2);
        studIds.put("Helmuth", 3);

        System.out.println(studIds);
        System.out.println("in Liste ist 1 enthalten? - "+ studIds.containsValue(1));
        System.out.println("Ist Helmuth in der Liste? - "+ studIds.containsKey("Helmuth"));
    }
}
