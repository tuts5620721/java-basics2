package dao;

import models.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentDAO {

    public List<Student> getStudent(){
        List <Student> students = new ArrayList<>();
        students.add(new Student("Sarah", 1, "Economics"));
        students.add(new Student("John", 2, "Biology"));
        students.add(new Student("Hans", 3, "Law"));
        return students;
    }
}
