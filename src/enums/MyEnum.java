package enums;

public class MyEnum {
    public static void main(String[] args) {

        //Alle Enums ausprinten
        for (Wochentage tage: Wochentage.values()){
            System.out.println(tage);
        }

        System.out.println("Freitag: " +Wochentage.FREITAG.kennNrTag);
    }
}
