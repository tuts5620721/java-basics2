package methods;

public class MyMethod {

    //hier Methoden usw
    public static void main(String[] args) {
        System.out.println("Ergebnis aus der add Funktion: ");
        System.out.println(add(3,6));

    }

    public static Integer add(int a, int b){
        return a+b;
    }

}
